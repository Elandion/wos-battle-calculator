import React from 'react';
import './App.css';
import Troops from './troops';
import { TroopEntry } from './types';


function setInput(element: HTMLInputElement, value: number) {
  element.value = value.toString();
}

function setStats(highValue: number, lowValue?: number) {
  const lethHealth = (lowValue !== undefined) ? lowValue : highValue;
  const attDef = highValue;
  document.querySelectorAll('input[name*=Att]').forEach(el => setInput(el as HTMLInputElement, attDef));
  document.querySelectorAll('input[name*=Def]').forEach(el => setInput(el as HTMLInputElement, attDef));
  document.querySelectorAll('input[name*=Leth]').forEach(el => setInput(el as HTMLInputElement, lethHealth));
  document.querySelectorAll('input[name*=Hlth]').forEach(el => setInput(el as HTMLInputElement, lethHealth));
}

function toggle() {
  const d = document.getElementById('statDiv') as HTMLDivElement;
  if (window.getComputedStyle(d).display === 'none') {
    d.style.display = 'block';
  }
  else {
    d.style.display = 'none';
  }
}


function App() {
  
  let attacker: TroopEntry[] = [];
  let defender: TroopEntry[] = [];
  function setAttack(data: TroopEntry[]) {
    attacker = data;
  }
  function setDefense(data: TroopEntry[]) {
    defender = data;
  }

  function compute() {
    alert('error 404 - magic not found')
    console.log(attacker.length);
    console.log(defender.length);
  }

  return (
    <div className="App">
      <header className="App-header">
        <span>
          Attacker
          <Troops beasts={false} changeCallback={setAttack} />
        </span>

        <span>
          Defender
          <Troops beasts={false} changeCallback={setDefense} />
        </span>
        <div>
          <br />
          <button onClick={compute}>FIGHT (do the magic)</button>
        </div>
        <div>
          <br /><br />
          <button onClick={toggle}>show/hide stats</button>
          <div id='statDiv' >
            <div>
              <span className='span50'>
                <table>
                  <tbody>
                    <tr>
                      <td>Inf Attack</td>
                      <td><input type='text' name='infAtt1'></input></td>
                    </tr>
                    <tr>
                      <td>Inf Defense</td>
                      <td><input type='text' name='infDef1'></input></td>
                    </tr>
                    <tr>
                      <td>Inf Lethality</td>
                      <td><input type='text' name='infLeth1'></input></td>
                    </tr>
                    <tr>
                      <td>Inf Health</td>
                      <td><input type='text' name='infHlth1'></input></td>
                    </tr>
                    <tr>
                      <td>Lanc Attack</td>
                      <td><input type='text' name='lanAtt1'></input></td>
                    </tr>
                    <tr>
                      <td>Lanc Defense</td>
                      <td><input type='text' name='lanDef1'></input></td>
                    </tr>
                    <tr>
                      <td>Lanc Lethality</td>
                      <td><input type='text' name='lanLeth1'></input></td>
                    </tr>
                    <tr>
                      <td>Lanc Health</td>
                      <td><input type='text' name='lanHlth1'></input></td>
                    </tr>
                    <tr>
                      <td>Mark Attack</td>
                      <td><input type='text' name='mrkAtt1'></input></td>
                    </tr>
                    <tr>
                      <td>Mark Defense</td>
                      <td><input type='text' name='mrkDef1'></input></td>
                    </tr>
                    <tr>
                      <td>Mark Lethality</td>
                      <td><input type='text' name='mrkLeth1'></input></td>
                    </tr>
                    <tr>
                      <td>Mark Health</td>
                      <td><input type='text' name='mrkHlth1'></input></td>
                    </tr>
                  </tbody>
                </table>
              </span>
              <span className='span50'>
                <table>
                  <tbody>
                    <tr>
                      <td>Inf Attack</td>
                      <td><input type='text' name='infAtt1'></input></td>
                    </tr>
                    <tr>
                      <td>Inf Defense</td>
                      <td><input type='text' name='infDef1'></input></td>
                    </tr>
                    <tr>
                      <td>Inf Lethality</td>
                      <td><input type='text' name='infLeth1'></input></td>
                    </tr>
                    <tr>
                      <td>Inf Health</td>
                      <td><input type='text' name='infHlth1'></input></td>
                    </tr>
                    <tr>
                      <td>Lanc Attack</td>
                      <td><input type='text' name='lanAtt1'></input></td>
                    </tr>
                    <tr>
                      <td>Lanc Defense</td>
                      <td><input type='text' name='lanDef1'></input></td>
                    </tr>
                    <tr>
                      <td>Lanc Lethality</td>
                      <td><input type='text' name='lanLeth1'></input></td>
                    </tr>
                    <tr>
                      <td>Lanc Health</td>
                      <td><input type='text' name='lanHlth1'></input></td>
                    </tr>
                    <tr>
                      <td>Mark Attack</td>
                      <td><input type='text' name='mrkAtt1'></input></td>
                    </tr>
                    <tr>
                      <td>Mark Defense</td>
                      <td><input type='text' name='mrkDef1'></input></td>
                    </tr>
                    <tr>
                      <td>Mark Lethality</td>
                      <td><input type='text' name='mrkLeth1'></input></td>
                    </tr>
                    <tr>
                      <td>Mark Health</td>
                      <td><input type='text' name='mrkHlth1'></input></td>
                    </tr>
                  </tbody>
                </table>
              </span>
            </div>
            <span>
              <button onClick={() => setStats(0)}>weak</button>
              <button onClick={() => setStats(20, 0)}>beginner</button>
              <button onClick={() => setStats(100, 20)}>middle</button>
              <button onClick={() => setStats(300, 180)}>decent</button>
              <button onClick={() => setStats(600, 450)}>strong</button>
              <button onClick={() => setStats(1000, 1000)}>godlike</button>
            </span>
          </div>
        </div>
      </header>
    </div>
  );
}

export default App;
