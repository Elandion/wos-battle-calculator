export enum TT {
    Infantry, Lancer, Marksman
}
export type TroopEntry = {
    type: TT;
    T: number;
    count: number;

    baseAttack?: number;
    baseDefense?: number;
    baseLethality?: number;
    baseHealth?: number;
    baseSpeed?: number;
}
