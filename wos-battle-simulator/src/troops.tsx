import React, { useState, useId } from 'react';

import Beasts from './Beasts';
import { TT, TroopEntry } from './types';

export default function Troops({ beasts, changeCallback }: { beasts: boolean, changeCallback: (data: TroopEntry[]) => void; }) {
    
    const [troopAmount, setTroops] = useState<TroopEntry[]>([]);
    const countId = useId();
    const levelId = useId();
    const typeId = useId();

    function addTroops(count: number, t: number, typ: TT) {
        const newTroop: TroopEntry = { T: t, count: count, type: typ };
        const newArr = troopAmount.slice(0);
        newArr.push(newTroop);
        setTroops(newArr);
        changeCallback(newArr);
    }

    function removeEntry(index: number) {
        const newArr = troopAmount.slice(0);
        newArr.splice(index, 1);
        setTroops(newArr);
        changeCallback(newArr);
    }

    function addHandler() {
        const c = (document.getElementById(countId) as HTMLInputElement).value;
        const t = (document.getElementById(levelId) as HTMLInputElement).value;
        const select = document.getElementById(typeId) as HTMLSelectElement;
        const typ = select.value;
        const typed = TT[typ as keyof typeof TT] //<keyof typeof TT>typ;
        const cNum = Number.parseInt(c);
        const tNum = Number.parseInt(t);
        if (cNum && tNum && typed) {
            addTroops(cNum, tNum, typed);
        }
    }

    const listItems = troopAmount.map((t, i) =>
        <li key={t.type + t.T}>
            {t.count + ' ' + t.type.toString() + ' T' + t.T} <button onClick={() => removeEntry(i)}>X</button>
        </li>
    );

    return (
        <div>
            <ul>{listItems}</ul>

            {beasts ? <Beasts /> : ''}
            <input id={countId} placeholder='Number of troops'></input>
            <input id={levelId} placeholder='T<x>'></input>
            <select id={typeId}>
                <option value={TT.Infantry}>Infantry</option>
                <option value={TT.Lancer}>Lancer</option>
                <option value={TT.Marksman}>Marksman</option>
            </select>
            <button onClick={addHandler}>add</button>

        </div>
    );
}
